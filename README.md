평범한 롤 5인큐용 봇입니다.

# 설치법

이 소스코드를 다운로드받습니다.

Token.env (확장자 .env 확인)파일명으로 파일을 만든 후 '디스코드 봇 토큰만 엔터 없이 1번째 라인에 넣습니다.'

소스코드 중 길드 ID, 카테고리 이름 등을 수정해야 합니다.

주석에 수정해야 하는 곳 표시해 놓겠습니다.

오류가 있으면 issue tracker로 표시 부탁드립니다.

Made By MisileLab

Youtube : https://www.youtube.com/channel/UCJpQZJtudbnTNjBayE832gg